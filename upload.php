<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <h2>ОТПРАВКА ФАЙЛОВ</h2>
    <form enctype="multipart/form-data" action="" method="post">
        <input name="userfile" type="file">
        <input type="submit" value="Отправить">
    </form>
    <hr>
    <?php
    $types_file = [
        'pdf' => 'application/pdf',
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'xls' => 'application/vnd.ms-excel',
        'jpg' => 'image/jpg',
        'png' => 'image/png'
    ];

    if (isset($_FILES['userfile'])) {
        if (isset($_FILES['userfile']['tmp_name'])) {
            $filename = basename($_FILES['userfile']['name']);
            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/upload/';
            $uploadfile = $uploaddir . $filename;

            $arr = explode('.', $filename);
            $file_type = strtolower(array_pop($arr));

            if (array_key_exists($file_type, $types_file)) {
                if ($types_file[$file_type] == $_FILES['userfile']['type']) {
                    move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
                    echo 'Файл успешно загружен';
                } else {
                    echo 'не допустимое расширение файла: ' . $_FILES['userfile']['type'];
                }
            } else {
                echo 'неправильный тип файла';
            }
        } else {
            echo 'не удалось передать файл на сервер';
        }
    } else {
        echo 'Файл не выбран';
    }
    echo "<pre>";
    print_r($_FILES);
    echo "</pre>";
    ?>

</body>

</html>